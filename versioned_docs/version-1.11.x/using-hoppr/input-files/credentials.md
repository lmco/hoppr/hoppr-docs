---
sidebar_position: 1
title: Credentials
---

# Credentials

The `Credentials` file specifies any credentials needed to access the `Manifest`, `SBOMs`, or `Components` that are access controlled.

A **Hoppr** specific YAML file that defines how to find credentials to access [Manifest](https://hoppr.dev/docs/using-hoppr/input-files/manifest.md), [SBOMs](https://hoppr.dev/docs/using-hoppr/tutorials/processing-101/#cyclonedx-sboms), and the components that will be collected.

In the example below, for `user_env` and `pass_env`: when the `_env` suffix is used, it will read it as an environment variable.

## Example `credentials.yml`

```yaml
---
schemaVersion: v1
kind: Credentials

metadata:
  name: Example Credentials File
  version: "1.0.0"
  description: This is an example credentials.yml file

credential_required_services:
   - url: https://hoppr.dev/ # Protocol is not required i.e hoppr.dev/
     user: jsmith1234
     pass_env: USER_TOKEN
```

## Credential Fields

The following provides a top level explanation of the fields available in the Credentials file.

### metadata

Provides information about the credentials file itself. All fields are optional.

| Name          | Description                  |
| :------------ | :--------------------------- |
| `name`        | Credentials file name        |
| `version`     | Credentials file version     |
| `description` | Credentials file description |

### credential_required_services

Specifies the credentials to use for a specific location, specified by URL.

Each item in the list requires the following fields:

| Name                 | Description                                          |
| :------------------- | :--------------------------------------------------- |
| `url`                | URL for which credentials should be provided         |
| `user` or `user_env` | Username or environment variable containing username |
| `pass_env`           | Environment variable containing password             |

Objects should exist for any location specified in other Hoppr configuration that requires authentication.

:::info[See also]

- [Manifest file documentation](../input-files/manifest.md)
- [Transfer file documentation](../input-files/transfer.md)

:::
