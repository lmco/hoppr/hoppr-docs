---
sidebar_position: 1
---

# Gitpod

Gitpod offers quick-start development environments. Use the following button if you would
like to develop and test without setting up a local development environment.

This web-based development environment has:

- A terminal with `poetry` and required python packages for development
- A terminal running `npm start` and serving the docs in a separate browser window

[Launch Hoppr in Gitpod](https://gitpod.io/#https://gitlab.com/hoppr/hoppr/-/tree/main/)

## Setup

Install [poetry](https://python-poetry.org/) to simplify package management and building.

```bash
# Install poetry and dependencies
$ pip install poetry
$ poetry install
```

## Testing

Any changes to the project need to pass three tests.  Our GitLab pipelines will also verify this during an MR.

New functionality should also be unit tested.

```bash
# Run unit tests, formatting cleanup, and test pep8
$ poetry run pytest ./hoppr
$ poetry run black ./hoppr
$ poetry run pylint ./hoppr
```

This project also uses `pre-commit` for identifying simple issues before submission to code review.  
You can run `pre-commit run --all-files` when your files are staged to ensure code consistency, but hooks are run on every commit automatically as well.  
You can view the full list of hooks run in [.pre-commit-config.yaml](https://gitlab.com/hoppr/hoppr/-/blob/main/.pre-commit-config.yaml).
