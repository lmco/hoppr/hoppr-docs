---
sidebar_position: 4
title: Writing Droppr Integration Tests
description: Writing Integration Tests for Droppr
---
# Writing Integration Tests

## Steps

1. Begin with a Hoppr Tar Bundle.  This is typically a bundle.tar.gz file. Be sure to document in the "git commit" where you got the file. A good place to find files is in hoppr/test/integration.
    - For APT, the file was obtained from hoppr/test/integration/IntTest_14 
    - One exception to this is Npm, which had to clone hoppr/test/integration/IntTest_12.
2. Create the directory structure under droppr/test/MODULE, with subdirectories filesys, local_install, and nexus (as appropriate). The subdirectories have the files .droppr.yml and expected-result-summary* files.
    - The .droppr.yml file can be derived from other files in droppr/test
    - The expected-result-summary* file takes some trial-and-error.
    One idea is to create a dummy file, commit to the branch, and the pipeline will show the expected vs. actual.
    Then carefully import the actual into the expected-result-summary* file
3. Use [MR "APT distributor code and integration test files"](https://gitlab.com/hoppr/droppr/-/merge_requests/112)  as an example.