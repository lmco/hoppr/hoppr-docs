---
sidebar_position: 2
title: Manifest
---

## Manifest

A **Hoppr** specific YAML file that specifies product dependencies required for product deployments. Could be provided by product teams
for use by end customers.

**Hoppr** currently supports the following [Package URLs](https://github.com/package-url/purl-spec/blob/master/PURL-TYPES.rst)
(PURLs) to find components. In `1.9.0` the following package types are supported:

| PURL Type | Description |
| --------- | ----------- |
| `cargo`   | cargo packages |
| `docker`  | container images |
| `generic` | stand-alone files (e.g. binary) not handled by a package manager |
| `git`     | git repositories and helm charts |
| `golang`  | golang packages |
| `helm`    | helm charts |
| `maven`   | maven packages |
| `npm`     | npm packages |
| `nuget`   | nuget packages |
| `pypi`    | python packages |
| `rpm`     | redhat RPMs (yum/dnf) |

```yaml
---
schemaVersion: v1
kind: manifest

metadata:
  name: Example Manifest File 
  version: "1.0.0" 
  description: This is an example manifest.yml file 

sboms:
  - url: https://example.com/example-bom.json
  - local: ../example-bom.json

includes:
  - url: https://example.com/example.yml
  - local: ../example.yml

repositories:
  docker:
    - url: registry.docker.io # Protocol is not required for registries
      description: Description for the Repo URL
    - url: https://example.com/
  pypi:
    - url: https://example.com/
      description: Description for the Repo URL
    - url: https://example2.com/
```

For more information on credential configuration options see the credentials file [documentation](../input-files/credentials.md)
For more information on transfer configuration options see the transfer file [documentation](../input-files/transfer.md)
