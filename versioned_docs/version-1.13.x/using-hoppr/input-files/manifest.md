---
sidebar_position: 2
title: Manifest
---

# Manifest

A **Hoppr** specific YAML file that specifies product dependencies required for product deployments. Could be provided by product teams
for use by end customers.

**Hoppr** currently supports the following [Package URLs](https://github.com/package-url/purl-spec/blob/master/PURL-TYPES.rst)
(PURLs) to find components. From version `1.10.0` onward the following package types are supported:

| PURL Type | Description                                                           |
| --------- | --------------------------------------------------------------------- |
| `cargo`   | cargo packages                                                        |
| `deb`     | Debian-based packages (only `apt`-based packages currently supported) |
| `docker`  | container images                                                      |
| `gem`     | RubyGems packages                                                     |
| `generic` | stand-alone files (e.g. binary) not handled by a package manager      |
| `git`     | git repositories and helm charts                                      |
| `golang`  | golang packages                                                       |
| `helm`    | helm charts                                                           |
| `maven`   | maven packages                                                        |
| `npm`     | npm packages                                                          |
| `nuget`   | nuget packages                                                        |
| `pypi`    | python packages                                                       |
| `raw`     | alias for `generic`                                                   |
| `rpm`     | redhat RPMs (yum/dnf)                                                 |

## Example `manifest.yml`

```yaml
---
schemaVersion: v1
kind: Manifest

metadata:
  name: Example Manifest File
  version: "1.0.0"
  description: This is an example manifest.yml file

sboms:
  - url: https://example.com/example-bom.json
  - local: ../example-bom.json

includes:
  - url: https://example.com/example.yml
  - local: ../example.yml

repositories:
  docker:
    - url: registry.docker.io # Protocol is not required for registries
      description: Description for the repository URL
    - url: https://example.com/
  pypi:
    - url: https://example.com/
      description: Description for the repository URL
    - url: https://example2.com/
```

## Manifest Fields

The following provides a top level explanation of the fields available in the Manifest file.

### metadata

Provides information about the manifest file itself. All fields are optional.

| Name          | Description               |
| :------------ | :------------------------ |
| `name`        | Manifest file name        |
| `version`     | Manifest file version     |
| `description` | Manifest file description |

### sboms

Locations of CycloneDX Software Bill of Materials files to be processed by Hoppr.

Each item in the list requires exactly one of the following fields:

| Name    | Description                                          |
| :------ | :--------------------------------------------------- |
| `local` | Path to SBOM file relative to manifest               |
| `oci`   | oci:// URL of remotely hosted SBOM file              |
| `url`   | http:// or https:// URL of remotely hosted SBOM file |

### includes

Additional manifest files that should be merged with this manifest to create the final file that Hoppr will utilize

Each item in the list requires exactly one of the following fields:

| Name    | Description                                              |
| :------ | :------------------------------------------------------- |
| `local` | Path to manifest file relative to this manifest          |
| `url`   | http:// or https:// URL of remotely hosted manifest file |

### repositories

Locations for Hoppr to use when pulling artifacts based on the PURL type in the SBOM

- While most PURL types define sane defaults for repositories if not specified by the user,
  it is recommended to explicitly define artifact source repositories
- Multiple repository URLs can be specified for each PURL type, and attempts to collect components
  from these repositories will occur in the order in which they are defined

:::info[See also]

- [Credentials file documentation](../input-files/credentials.md)
- [Transfer file documentation](../input-files/transfer.md)

:::
