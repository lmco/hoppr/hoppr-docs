---
sidebar_position: 1
title: Droppr Set-Up
description: How to Set-Up Droppr for Development
---

# General set-up
If you have not done so yet, set up a Linux machine.

Clone the [Droppr git repository](https://gitlab.com/hoppr/droppr/).

# Set up for Go development
It is recommended to update to the latest version of Go for development. Confirm go version:
```bash
go version
```

Build droppr:
```bash
cd droppr
go build -o bin/droppr -ldflags="-X 'gitlab.com/hoppr/droppr/cmd.Version=0.1.2-cac'"
```
Optional: [run Droppr unit tests](/docs/droppr/development/droppr-unittests.md).



At this point, you can do droppr development!

Optionally, verify that an integration test works.

Example:

Run the Python filesys test locally (one still needs to perform manual assertions; this just populates the target_dir)
```bash
cd test/Python/filesys/
../../../bin/droppr install -b ../bundle.tar.gz
```

# Installing a different version of Go
If your Linux Machine has an older version of Go, upgrade with the following command:
```bash
wget https://go.dev/dl/go1.20.4.linux-amd64.tar.gz
sudo rm -rf /usr/local/go && sudo tar -C /usr/local -xzf go1.20.4.linux-amd64.tar.gz
```
