import React from 'react';
import { DistributorList } from './data';

function Type({name}) : JSX.Element {
    return (
        <code className="padding--sm margin-right--sm">{name}</code>
    );
  }


export default function Distributor({children, distributorName, showDefaultExample = true}) : JSX.Element {
    var distributor = DistributorList.find(p => p.distributorName.toLowerCase() === distributorName.toLowerCase());
    return (
      <div style={{margin: '2rem 0 0 0'}}>
          <h2>Details</h2>
          <div className="card">
              <div className="card__body">
              <div className="padding-left--md">
                  <h4 className="padding-top--md">Distributor</h4>
                  <div className="padding-left--md padding-bottom--md">
                      <code className="padding--sm">{distributorName}</code>
                  </div>
                  <h4>Supported PURL Types</h4>
                  <div className="padding-left--md padding-bottom--md">
                  {distributor.supportedPurl.length > 0 ? 
                        distributor.supportedPurl.map(purl => <Type name={purl} />) :
                        <code className="padding--sm margin-right--sm">NONE</code>
                      }
                  </div>
                  <h4>Supported Install Types</h4>
                  <div className="padding-left--md padding-bottom--md">
                      {distributor.supportedInstallTypes.length > 0 ? 
                        distributor.supportedInstallTypes.map(type => <Type name={type} />) :
                        <code className="padding--sm margin-right--sm">NONE</code>
                      }
                  </div>
              </div>
              </div>
          </div>
      </div>
    );
  }