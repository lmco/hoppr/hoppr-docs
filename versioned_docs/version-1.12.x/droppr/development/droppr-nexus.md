---
sidebar_position: 5
title: Nexus
description: Installing/Starting Nexus
---

## Install/Start

***Linux Machine***
```bash
cd droppr
./scripts/start_nexus.sh
```
The file start_nexus.sh contains the default password for admin: see variable "_REQ_PW"
- If this message occurs:
```
docker: Error response from daemon: Conflict. The container name "/mynexus" is already in use by container
```
follow the steps at [Full Clean](#full-clean).

***Browser***
- Confirm that Nexus is running

<http://localhost:8081>



To manually run a test using Nexus:


***Linux Machine***
```bash
export NEXUS_PW=<admin_password>
```
where <admin_password> is determined during the install.
For the default <admin_password>, look in file droppr/scripts/start_nexus.sh
- Manually run a test (there will be no assertions)

Example:
```bash
cd droppr/test/Helm/nexus
../../../bin/droppr install -b ../bundle.tar.gz
```

***Browser***
- Confirm the Nexus upload worked:

<http://localhost:8081>

- There is residue left over from the test. If you try to run a different nexus test, e.g., for Maven, it will fail.
- If one wishes to test a different format type, see [Removing the droppr-test-repo](#removing-the-droppr-test-repo).


## Removing the droppr-test-repo

### Symptom: Repository droppr-test-repo has different format

This can happen when working on the local Linux Machine.

For example:
```bash
$ cd droppr/test/Python/nexus
$ ../../../bin/droppr install -b ../bundle.tar.gz
Beginning install process on bundle ../bundle.tar.gz
   [ 25%] Component pkg:pypi/apispec@5.2.2 failed: Repository droppr-test-repo has format 'apt', format 'pypi' requested
...
```

This should not happen in the pipeline because one gets a fresh Nexus for every test case.

### Web UI Clean

This is a quicker way to perform the clean.

***Browser***
- Logon to Nexus as admin

<http://localhost:8081>

- Click on the Settings icon in the top navigation bar
- Repository → Repositories
- Click on "droppr-test-repo"
- Click on "Delete repository"

### Full Clean
This performs a full clean by destroying the docker container and creating a new one

***Linux Machine***
- Remove the Nexus container 
```bash
docker container ls -a
docker stop CONTAINER_ID
docker rm CONTAINER_ID
```	
- Start up nexus
```bash
cd droppr
./scripts/start_nexus.sh
```
