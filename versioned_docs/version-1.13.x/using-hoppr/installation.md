---
sidebar_position: 1
---

# Installation

## Docker Run

You can run hoppr with the `hoppr/hopctl` [docker image](./tutorials/hoppr-docker-image.md).

```bash
$ docker run hoppr/hopctl version
Hoppr Framework Version: {{ hoppr.version }}
Python Version: 3.10.4
```

## Install Hoppr CLI

:::note Req #1 - Python

__Python 3.10__ or higher is required to run Hoppr.  Instructions to download Python are [available here](https://www.python.org/downloads/).

For Rocky Linux users, Python may need to be built from source.

:::

Hoppr is available on [pypi.org](https://pypi.org/project/hoppr/).  You can install Hoppr simply by running the following commands:

```bash
# Instantiate virtual environment
python3 -m venv .venv

# Source virtual environment
source .venv/bin/activate

# Install hoppr
python3 -m pip install hoppr
```

You can verify that Hoppr is installed by executing `hopctl version`:

```bash
$ hopctl version
Hoppr Framework Version: {{ hoppr.version }}
Python Version: 3.10.4
```

## Plugin Dependencies

Hoppr plugins often utilize underlying system applications.  This means that some plugins will only run
on certain Operating Systems.  For example, the `DNF` and `YUM` collectors generally require a RHEL-based platform.  Some examples:

* ```collect_dnf_plugin``` requires __dnf__
* ```collect_docker_plugin``` requires __skopeo__ ([install instructions](#install-skopeo))
* ```collect_git_plugin``` requires __git__ ([install instructions](#install-git))
* ```collect_helm_plugin``` requires __helm__ ([install instructions](#install-helm))
* ```collect_maven_plugin``` requires __mvn__ ([install instructions](#install-maven))
* ```collect_pypi_plugin``` requires __pip__ ([install instructions](#install-pip))
* ```collect_yum_plugin``` requires __yumdownloader__

### Install skopeo

```bash
# Ubuntu
apt-get install --yes skopeo

# Rocky
dnf install --assumeyes skopeo

# Alpine
apk add --no-cache skopeo
```

### Install git

```bash
# Ubuntu
apt-get install --yes git

# Rocky
dnf install git-all

# Alpine
apk add --no-cache git
```

### Install helm

```bash
# Rocky
dnf install --assumeyes helm

# Alpine/Ubuntu
curl -fsSL https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3 | bash
```

### Install maven

```bash
# Ubuntu
apt-get install --yes maven

# Rocky
dnf install --assumeyes maven

# Alpine
apk add --no-cache maven
```

### Install pip

```bash
# Ubuntu
apt-get install python3-pip

# Rocky
dnf install python3-pip

# Alpine
apk add py3-pip

# Without package manager
python3 -m ensurepip
```
