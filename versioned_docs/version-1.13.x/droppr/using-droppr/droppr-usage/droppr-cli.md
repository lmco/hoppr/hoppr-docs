---
sidebar_position: 3
title: Droppr CLI
description: How to use Droppr
---

# Droppr CLI

## Install Command

The Droppr install command takes the form of 
```bash
droppr install [flags]

Flags:
  -b, --bundle string    Hoppr bundle to install (required)
  -h, --help             help for install
  -l, --logfile string   Log file location (default "droppr.log")
      --config string    config file (default is .droppr.yml or $HOME/.config/.droppr.yml)
      --num_workers int  set number of worker threads for processing (default 10)
```
If the `config` file is not specified, the program first looks for a `.droppr.yml` file in the current working directory.  If not found there, it looks in `$HOME/.config/.droppr.yml`.

If `num_workers` (the number of worker threads) is not specified on the command line, the value may be specified in the `config` file.  If not found in either of those locations, a default value of 10 is used.

For example:
```bash
droppr install --bundle bundle.tar.gz --logfile droppr_log.txt --config droppr_config.yml
```
