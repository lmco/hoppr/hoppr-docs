type DistributorItem = {
    distributorName: string;
    supportedPurl: string[];
    supportedInstallTypes: string[];
  };

export const DistributorList: DistributorItem[] = [
    { distributorName: "APT", supportedPurl: ["deb"], supportedInstallTypes: ["local_install", "filesys", "nexus"] },
    { distributorName: "Docker", supportedPurl: ["docker"], supportedInstallTypes: ["local_install", "filesys", "nexus", "oci_registry"] },
    { distributorName: "Generic", supportedPurl: ["generic"], supportedInstallTypes: ["filesys", "nexus"] },
    { distributorName: "Git", supportedPurl: ["git","gitlab"], supportedInstallTypes: ["local_install", "filesys", "nexus"] },
    { distributorName: "Helm", supportedPurl: ["helm"], supportedInstallTypes: ["filesys", "nexus", "oci_registry"] },
    { distributorName: "Maven", supportedPurl: ["maven"], supportedInstallTypes: ["local_install", "filesys", "nexus"] },
    { distributorName: "Npm", supportedPurl: ["npm"], supportedInstallTypes: ["filesys", "nexus"] },
    { distributorName: "NuGET", supportedPurl: ["nuget"], supportedInstallTypes: ["filesys", "nexus"] },
    { distributorName: "Python", supportedPurl: ["pypi"], supportedInstallTypes: ["local_install", "filesys", "nexus"] },
    { distributorName: "Rpm", supportedPurl: ["rpm"], supportedInstallTypes: ["local_install", "filesys", "nexus"] }
];