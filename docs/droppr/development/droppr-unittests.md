---
sidebar_position: 2
title: Droppr Unit Tests
description: Running Unit Tests for Droppr
---

# Running the unit test:
## On your Linux machine
```bash
cd droppr
go test -v ./... -coverprofile=coverage.out -covermode count
```

## To get code coverage information:
```bash
go tool cover -func=coverage.out
```

# Pipeline run
The pipeline run, job gobuild:unit-test, runs the unit tests and gives similar code coverage information.