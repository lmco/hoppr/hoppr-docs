---
sidebar_position: 5
title: Git Bundle Collector
---

import Plugin from '@site/src/components/Plugins';

<Plugin transferTag="git_bundle" showDefaultExample={false} />

# Hoppr Git Bundle Collector Plugin

The [Hoppr Git Bundle Collector Plugin](https://gitlab.com/hoppr/plugins/hoppr-git-bundle-collector-plugin)
can be used to collect Git objects and refs (identified by SBOM components with `git`, `gitlab` and `github`
purl types) in the [Git bundle format](https://git-scm.com/docs/gitformat-bundle).

## Configuration Options

The plugin supports the following configuration options. All are optional.

| Option             | Details                                                                                                                                          | Default Value              |
| ------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------ | -------------------------- |
| `git_command`      | The git executable to use.                                                                                                                       | git                        |
| `temp_dir`         | The directory to use for temporary storage.                                                                                                      | `<system temp directory>`  |
| `branch`           | The branch to bundle. Overrides branch names specified via the purl version field or via the component version field                             |                            |
| `bundle_name`      | The name of the bundle file that will be created.                                                                                                | `<repo>`-`<branch>`.bundle |
| `since`            | A time reference (e.g. 10.days.ago) that will be used to establish the bundle basis.  Mutually exclusive with `previous_commits` and `qualifier` |                            |
| `previous_commits` | The number of previous commits to include in the bundle. Mutually exclusive with `since` and `qualifier`                                         |                            |
| `qualifier`        | Purl [qualifier][PURL spec] whose value contains the tag or branch to use as the basis. Mutually exclusive with `previous_commits` and `since`   |                            |

### Bundle Name

The bundle name will convert `<repo>` to the name of the git repository and `<branch>` to the
branch that is being bundled.

### Bundle Basis

By default all commits for the branch will be included in the bundle. The three basis options
can be used to reduce the number of commits that will be in the bundle. At most one basis option
should be specified. If multiple are specified then `since` has the highest precedence,
`previous_commits` has the next highest precedence, and `qualifier` has the lowest precedence.

#### Since

The `since` basis can be used to limit the number of commits included in the bundle by specifying
a minimum date. The format is free-form and is passed in to the `--since` arg of `git bundle`. Both
fixed dates (e.g. 7/17/23) and relative dates (e.g. today, 1.month.ago, last.Tuesday) are supported.

#### Previous Commits

The `previous_commits` basis can be used to limit the number of commits included in the bundle by
specifying the maximum number of commits. Any positive integer is valid. If only the previous 100
commits are desired then set `previous_commits` to 100.

#### Qualifier

The qualifier option will tell the plugin to retrieve the basis from a qualifier on the purl. For example,
if the qualifier was set to `previous` and the purl string was `pkg:gitlab/hoppr/hoppr@dev?previous=v1.8.0`,
then only commits on the `main` branch that are not in the `v1.8.0` tag would be included in the bundle.
If the specified qualifier is not found then all commits on the branch will be included in the bundle.

## How to use this plugin

### Install the plugin alongside hoppr

```bash
$ python3 -m venv .venv
$ source .venv/bin/activate
(.venv) $ pip install hoppr hoppr-git-bundle-collector
```

### Create a transfer file

A [Hoppr transfer file](../input-files/transfer) defines the stages to be run and which plugins will be run in those stages.

Create a transfer file that defines a stage with this plugin (identified by `git_bundle`) specified. For example:

```yaml
---
schemaVersion: v1
kind: Transfer

stages:
  Collect:
    plugins:
      - name: git_bundle
        config:
          since: 7.days

max_processes: 10
```

### Run Hoppr

That's everything! Run `hopctl bundle` as normal and the plugin will create a
bundle for any `git`, `gitlab`, or `github` purl types listed in the SBOMs.

[PURL spec]: https://github.com/package-url/purl-spec/blob/master/PURL-SPECIFICATION.rst#:~:text=such%20comparison%20consistently.-,qualifiers,-%3A
