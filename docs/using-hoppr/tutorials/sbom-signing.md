---
sidebar_position: 7
---

# SBOM Signing

Hoppr leverages [`cryptography`](https://cryptography.io/en/latest/) to provide SBOM signing capabilities.

Only RSA, Ed25519, and EllipticCurve private key types are currently supported. This follows the same process required by the [`attestation`](./attestation.md#generate-functionary-key) generation tooling.

## Usage

### Signing a bundle

Run the `hopctl bundle` subcommand as usual, additionally specifying the `--sign`, `--functionary-key`, and `--functionary-key-password` (if the private key is secured with a passphrase) options.

```shell
hopctl bundle --transfer transfer.yml --log hoppr_log.txt --verbose --sign --functionary-key functionary_key --functionary-key-password functionary-key-password manifest.yml
```

This will create the expected bundle that also includes the signature file `<bundle_name>.sig`.

### Examples

```shell
# Perform a bundle with a generated RSA private key
openssl genrsa -traditional -out functionary_key 3072
openssl rsa -in functionary_key  -outform PEM -pubout -out functionary_key.pub
hopctl bundle --transfer transfer.yml --log hoppr_log.txt --verbose --sign --attest --functionary-key mykey.pem manifest.yml
```
