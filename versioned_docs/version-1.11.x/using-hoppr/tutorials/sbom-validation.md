---
sidebar_position: 7
---

# SBOM Validation Overview

The `hopctl validate sbom` command (introduced in Hoppr v1.11.0) is used to validate that an SBOM:

- is using the current latest CycloneDX specification version
- is populated with the [minimum fields recommended by NTIA](https://www.ntia.doc.gov/files/ntia/publications/sbom_minimum_elements_report.pdf)
- has valid license data with no expired or soon-to-expire licenses

The command can be configured to simply emit warnings or optionally return a failing exit code
using various strictness flags when missing fields or invalid values are encountered.

## Command Line Options

```console
 Usage: hopctl validate sbom [OPTIONS]

 Validate SBOM file(s)

| Options                      | Flag | Type      | Description                                                                         |
| ---------------------------- | ---- | --------- | ----------------------------------------------------------------------------------- |
| --sbom                       | -s   | FILE      | Path to SBOM file (can be specified multiple times)                                 |
| --sbom-dir                   | -d   | DIRECTORY | Directory containing SBOM files (can be specified multiple times)                   |
| --strict                     | -S   |           | Enable all strict validation options                                                |
| --strict-ntia-minimum-fields | -n   |           | Raise error if minimum fields recommended by NTIA are not set                       |
| --strict-license-fields      | -l   |           | Raise error if SBOM license or SBOM/component license expiration fields are not set |
| --expiration-days            | -d   | FLOAT     | Number of days allowed by license expiration check                                  |
| --output-format              | -f   | [json]    | Format for output file [default: json]                                              |
| --output-file                | -o   | FILE      | Path to output file                                                                 |
| --help                       | -h   |           | Show this message and exit.                                                         |
```

## Checks

:::note
Components of type `operating-system` are not evaluated since they often don't contain the required fields.
:::

### NTIA Minimum Recommended Fields

The following checks will be performed on the SBOM document and its metadata:

- Is this SBOM using the latest CycloneDX specification version?
- Does the SBOM specify the `metadata` field?
  - Does the SBOM metadata specify one of the `authors` or `tools` fields?
  - Does the SBOM metadata specify the `timestamp` field?

The following checks will be performed on each component in the SBOM's `components` array:

- Does the component specify the `supplier` field?
- Does the component specify the `name` field?
- Does the component specify the `version` field?
- Does the component specify one of the unique identifier fields `cpe`, `purl`, or `swid`?

### License Fields

The following checks will be performed on the SBOM's `metadata` field and each component in the `components` array:

- Does the metadata or component specify the `licenses` field?

The following checks will be performed on each license in the metadata or component `licenses` array:

- Does the license specify one of the `id` or `name` fields?
- Does the license specify the `licensing` field?

### License Expiration

The following checks will be performed on each license in the metadata or component `licenses` array:

- If present, does the `licensing` field specify the `expiration` field?
- Is the date in the `expiration` field in the past (expired), or within the minimum allowable number of days in the future (about to expire)?

:::note
The minimum allowable number of days for a license's expiration to pass
validation is configured via the `--expiration-days` option (default: 30)
:::

### Strictness Flags

The command line options `--strict`, `--strict-ntia-minimum-fields`, and `--strict-license-fields`
control whether a validation failure will be displayed as a warning or a failure, as well as
whether to return a failing exit code of `1` upon completion.

Validating with no strictness options specified (warn only) will still report validation errors,
but will be logged in the log file at the `WARN` log level, displayed with a warning symbol in the
result summary, and return an exit code of `0`.

## Examples

The following SBOM file named `acme.cdx.json` will be used in the subsequent examples:

```json
{
  "$schema": "http://cyclonedx.org/schema/bom-1.5.schema.json",
  "bomFormat": "CycloneDX",
  "specVersion": "1.5",
  "serialNumber": "urn:uuid:b6f4e926-6a94-4209-960f-c7045f4e3440",
  "version": 1,
  "metadata": {
    "timestamp": "2023-11-27T23:57:56+00:00",
    "licenses": [
      {
        "license": {
          "name": "Acme Software License",
          "url": "https://acme-software.com/licenses/LICENSE.txt"
        }
      }
    ],
    "tools": [],
    "component": {
      "bom-ref": "pkg:oci/python@sha256%3A31ceea009f42df76371a8fb94fa191f988a25847a228dbeac35b6f8d2518a6ef?arch=amd64\u0026repository_url=index.docker.io%2Flibrary%2Fpython",
      "type": "container",
      "name": "Acme Software",
      "version": "1.0.0",
      "purl": "pkg:oci/python@sha256%3A31ceea009f42df76371a8fb94fa191f988a25847a228dbeac35b6f8d2518a6ef?arch=amd64\u0026repository_url=index.docker.io%2Flibrary%2Fpython"
    }
  },
  "components": [
    {
      "bom-ref": "cd42f200-197b-41df-9d39-f86ce6e8dce4",
      "type": "operating-system",
      "name": "debian",
      "version": "12.2"
    },
    {
      "bom-ref": "pkg:deb/debian/adduser@3.134?arch=all\u0026distro=debian-12.2",
      "type": "library",
      "supplier": {
        "name": "Debian Adduser Developers \u003cadduser@packages.debian.org\u003e"
      },
      "name": "adduser",
      "version": "3.134",
      "licenses": [
        {
          "license": {
            "name": "GPL-2.0"
          }
        }
      ],
      "purl": "pkg:deb/debian/adduser@3.134?arch=all\u0026distro=debian-12.2"
    },
    {
      "bom-ref": "pkg:deb/debian/apt@2.6.1?arch=amd64\u0026distro=debian-12.2",
      "type": "library",
      "supplier": {
        "name": "APT Development Team \u003cdeity@lists.debian.org\u003e"
      },
      "name": "apt",
      "version": "2.6.1",
      "licenses": [
        {
          "license": {
            "name": "GPL-2.0"
          }
        },
        {
          "license": {
            "name": "BSD-3-Clause"
          }
        },
        {
          "license": {
            "name": "Expat"
          }
        }
      ],
      "purl": "pkg:deb/debian/apt@2.6.1?arch=amd64\u0026distro=debian-12.2"
    }
  ]
}
```

### Warn Only

```shell
$ hopctl validate sbom --sbom acme.cdx.json --log hoppr.log --basic-term
Validating acme.cdx.json...

======================================================== Summary =========================================================
Results for acme.cdx.json:
    CycloneDX Specification Version                                     ✔
    Minimum NTIA Fields                                                 ⚠
    1 out of 1 SBOM licenses missing minimum license fields             ⚠
    1 out of 1 SBOM licenses expired or expiring within 30 days         ⚠
    0 out of 2 components missing minimum NTIA fields                   ✔
    4 out of 4 component licenses missing minimum license fields        ⚠
    4 out of 4 component licenses expired or expiring within 30 days    ⚠
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
See log file hoppr.log for full results.

$ echo $?
0
```

### Enforce NTIA Minimum Recommended Fields

```shell
$ hopctl validate sbom --sbom acme.cdx.json --strict-ntia-minimum-fields --log hoppr.log --basic-term
Validating acme.cdx.json...

======================================================== Summary =========================================================
Results for acme.cdx.json:
    CycloneDX Specification Version                                     ✔
    Minimum NTIA Fields                                                 ❌
    1 out of 1 SBOM licenses missing minimum license fields             ⚠
    1 out of 1 SBOM licenses expired or expiring within 30 days         ⚠
    0 out of 2 components missing minimum NTIA fields                   ✔
    4 out of 4 component licenses missing minimum license fields        ⚠
    4 out of 4 component licenses expired or expiring within 30 days    ⚠
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
See log file hoppr.log for full results.

$ echo $?
1
```

### Enforce Required License Fields

```shell
$ hopctl validate sbom --sbom acme.cdx.json --strict-license-fields --log hoppr.log --basic-term
Validating acme.cdx.json...

======================================================== Summary =========================================================
Results for acme.cdx.json:
    CycloneDX Specification Version                                     ✔
    Minimum NTIA Fields                                                 ⚠
    1 out of 1 SBOM licenses missing minimum license fields             ❌
    1 out of 1 SBOM licenses expired or expiring within 30 days         ❌
    0 out of 2 components missing minimum NTIA fields                   ✔
    4 out of 4 component licenses missing minimum license fields        ❌
    4 out of 4 component licenses expired or expiring within 30 days    ❌
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
See log file hoppr.log for full results.

$ echo $?
1
```

### Enforce All Validation Checks

The `--strict` option is equivalent to including both the `--strict-ntia-minimum-fields` and `--strict-license-fields` options.

```shell
$ hopctl validate sbom --sbom acme.cdx.json --strict --log hoppr.log --basic-term
Validating acme.cdx.json...

======================================================== Summary =========================================================
Results for acme.cdx.json:
    CycloneDX Specification Version                                     ✔
    Minimum NTIA Fields                                                 ❌
    1 out of 1 SBOM licenses missing minimum license fields             ❌
    1 out of 1 SBOM licenses expired or expiring within 30 days         ❌
    0 out of 2 components missing minimum NTIA fields                   ✔
    4 out of 4 component licenses missing minimum license fields        ❌
    4 out of 4 component licenses expired or expiring within 30 days    ❌
──────────────────────────────────────────────────────────────────────────────────────────────────────────────────────────
See log file hoppr.log for full results.

$ echo $?
1
```
