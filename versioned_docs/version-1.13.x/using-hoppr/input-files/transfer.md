---
sidebar_position: 3
title: Transfer
---

# Transfer

The `Transfer` file will specify the processing steps needed to collect, augment, filter, report, and bundle components.

A **Hoppr** specific YAML file that defines how to run a series of stages and plugins used to collect, process, and bundle components.

## Default Component Coverage

There are configuration options to define how frequently you want each SBOM component to be processed using the `default_component_coverage` variable.

| Variable            | Description                                                      |
| ------------------- | ---------------------------------------------------------------- |
| `OPTIONAL`          | Processing is optional, but can be any number of times           |
| `EXACTLY_ONCE`      | The component will be processed exactly one time                 |
| `AT_LEAST_ONCE`     | The component has to be processed at least once, but can be more |
| `NO_MORE_THAN_ONCE` | The component can be processed either once or 0 times            |

## Specifying Plugins

Users have the ability to specify plugins in the transfer file in three ways:

- Short module name: `collect_pypi_plugin`
- Plugin class name: `CollectPypiPlugin`
- Full module path: `hoppr.core_plugins.collect_pypi_plugin`

## Example `transfer.yml`

```yaml
---
schemaVersion: v1
kind: Transfer

stages:
  Collect:
    plugins:
      - name: hoppr.core_plugins.collect_docker_plugin
      - name: hoppr.core_plugins.collect_git_plugin
      - name: hoppr.core_plugins.collect_helm_plugin
      - name: hoppr.core_plugins.collect_maven_plugin
      - name: hoppr.core_plugins.collect_pypi_plugin
        config:
          pip_command: pip3
      - name: hoppr.core_plugins.collect_raw_plugin
  Bundle:
    plugins:
      - name: hoppr.core_plugins.bundle_tar
        config:
          tarfile_name: ~/tarfile.tar.gz

max_processes: 3
```

## Transfer Fields

The following provides a top level explanation of the fields available in the Transfer file.

### metadata

Provides information about the transfer file itself. All fields are optional.

| Name          | Description               |
| :------------ | :------------------------ |
| `name`        | Transfer file name        |
| `version`     | Transfer file version     |
| `description` | Transfer file description |

### max_processes

Specifies the max number of processes for Hoppr to utilize when running.

### stages

Specifies stages by name and which plugins to enable per stage.

Stage names are user specified, such as `Collect` and `Bundle` in the above example.

#### plugins

Specifies the plugins and optional configuration values for each plugin in the stage.

| Name     | Description                                                     |
| :------- | :-------------------------------------------------------------- |
| `name`   | Plugin name, see [Specifying Plugins](#specifying-plugins)      |
| `config` | Mapping of configuration keys and values accepted by the plugin |

:::info[See also]

- [Credentials file documentation](../input-files/credentials.md)
- [Manifest file documentation](../input-files/manifest.md)

:::
