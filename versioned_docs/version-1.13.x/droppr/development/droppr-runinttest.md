---
sidebar_position: 3
title: Running Droppr Integration Tests
description: Running Integration Tests for Droppr
---
# Running Integration Tests

The integration tests perform the full run in the pipeline on gitlab.com.

The test specification is in file droppr/ci/integration-test.yml. However, the integration-test.yml can not be "run" in the Linux Machine development environment. Instead, one can run a segment of it by doing something like this:
```bash
cd droppr/test/Python/filesys
../../../bin/droppr install -b ../bundle.tar.gz
```

This does not run the assertions, but generates artifacts to continue test development.  Note that files that are created are in the droppr/test directory structure, so be sure to not do a "git add" on these files.

Continuing the example, one file is created, droppr.log and one directory structure is created, target_dir. Make appropriate modifications to your branch, then push the branch to gitlab.com, where the modified code causes the pipeline to run. Below is an example of what a pipeline run looks like:

<div style={{ textAlign: 'center' }}>
  <img style={{ maxHeight: '400px'}} src="/img/droppr-img/droppr_integration_pipeline.png" />
</div>